#! /bin/bash

set -e # exit on errors


function print_usage ()
{
    echo ""
    echo "This script build all/one of the relevant cc33xx software package(s)."
    echo ""
    echo "Usage : "
    echo ""
    echo "Building full package : "
    echo " ./build_cc33xx.sh patch_kernel		[ Add cc33xx driver and configuration to PSDK's kernel ]"
    echo " ./build_cc33xx.sh build_kernel		[ Build PSDK's kernel ]"
    echo " ./build_cc33xx.sh install_kernel		[ Install PSDK's kernel ]"

    exit 1
}


function main()
{
	case "$1" in
		'patch_kernel')                
		echo "Inserting CC33XX driver into Processor SDK"
		patch -p0 --directory="../board-support/ti-linux-kernel-6.1.46+gitAUTOINC+1d4b5da681-g1d4b5da681/" < patches/cc33xx_kernel.patch
		echo "Inserting CC33XX configuration into Processor SDK"
		patch -p0 --directory="../board-support/ti-linux-kernel-6.1.46+gitAUTOINC+1d4b5da681-g1d4b5da681/" < patches/cc33xx_config.patch
		;;
			  
		'build_kernel')
		cd ..

		GCC_VER=$(gcc --version | grep gcc | awk '{print $4}')
		if [[ $GCC_VER == "4.8.5" ]]; then
				echo "Overriding host compiler with GCC-9"
				HOST_COMPILER="HOSTCC=gcc-9 HOSTCXX=g++-9"
		else
				HOST_COMPILER=""
		fi

		make linux $HOST_COMPILER KERNEL_DEVICETREE_UNFILTERED=am335x-boneblack-cc33xx-boosterpack.dtb
		;;

		'install_kernel')
		cd ..
		ROOTFS=$(pwd)/cc33xx/cc33xx_rootfs
		install -d $ROOTFS/boot
		make linux_install DESTDIR=$ROOTFS/boot  KERNEL_DEVICETREE_UNFILTERED=am335x-boneblack-cc33xx-boosterpack.dtb
		make linux_modules_install -o linux DESTDIR=$ROOTFS 
		cp $ROOTFS/boot/am335x-boneblack-cc33xx-boosterpack.dtb $ROOTFS/boot/am335x-boneblack.dtb
		;;

		'-h')
		print_usage
		;;

		'--help')
		print_usage
		;;

		*)
		echo "**** Unknown action '$1' - please see usage below **** "
		print_usage
		;;

	esac

	return 0
}


main $@