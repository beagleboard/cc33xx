
################################### Important ##################################
# This script assuems libnl was configured and built. Please provide the paths
# to it's includes and compiled libraries via the following paths:
NL_INCLUDE=                                     # <path>/include/libnl3
NL_LIB=                                         # Where libnl-genl-3.a can be found
# Specify where the toolchain used for cross-compiling is located:
export TOOLCHAIN_PATH=                          # <path>/arm/bin
export CROSS_COMPILE=                           # compiler prefix (arm-linux-gnueabihf-  or simillar)
################################################################################

#### Internal variables ####
export ARCH=arm
export PATH=$TOOLCHAIN_PATH:$PATH

NLVER=3 NL_INCLUDE=$NL_INCLUDE NL_LIB=$NL_LIB LIBS+=-lpthread   make
